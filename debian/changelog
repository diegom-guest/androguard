androguard (3.3.5-3) UNRELEASED; urgency=medium

  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 04 Jan 2020 23:03:30 +0000

androguard (3.3.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * debian/rules
    - run unittests at build-time
  * debian/patches/networkx-2.4.patch
    - compatiblity with networkx 2.4; Closes: #945390

 -- Sandro Tosi <morph@debian.org>  Tue, 31 Dec 2019 20:20:28 -0500

androguard (3.3.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Hans-Christoph Steiner ]
  * New upstream version
  * audroauto was removed by upstream

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Oct 2019 20:54:46 +0200

androguard (3.3.3-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 28 Jan 2019 12:10:36 +0100

androguard (3.3.1-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 25 Jan 2019 15:49:45 +0000

androguard (3.2.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Hans-Christoph Steiner ]
  * New upstream version
  * enable full test suite in autopkgtest

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 25 Sep 2018 12:41:58 +0200

androguard (3.1.0~rc2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python3-Version field

  [ Hans-Christoph Steiner ]
  * disable tests that fail on big-endian and re-enable on those arches
  * Standards-Version: 4.1.4 no changes

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 16 May 2018 20:21:04 +0100

androguard (3.1.0~rc2-1) unstable; urgency=medium

  * New upstream version 3.1.0~rc2

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 21 Feb 2018 20:21:04 +0100

androguard (3.1.0~rc1-2) unstable; urgency=medium

  * disable big-endian arches (Closes: #890593)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 19 Feb 2018 22:09:47 +0100

androguard (3.1.0~rc1-1) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * New upstream versions
  * Switch from python2 to python3
  * remove .py from commands

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 13 Feb 2018 10:19:36 +0100

androguard (2.0-3) unstable; urgency=medium

  [ Bhavani Shankar ]
  * upstream commit e1bd2a7 to fix compilation on non-x86 (Closes: #849647)

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 11 Mar 2017 10:17:40 +0100

androguard (2.0-2) unstable; urgency=medium

  * lintian overrides: the included binaries are test objects
  * manually include python-networkx (Closes: #823309)
  * include Vcs tags
  * Uploaders: Debian Python Modules Team
  * Standards-Version: 3.9.8, no changes
  * remove pointless README (Closes: #824978)

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 01 Jun 2016 12:08:23 +0200

androguard (2.0-1) unstable; urgency=low

  * Initial release. (Closes: #811421)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 18 Jan 2016 20:54:01 +0100
